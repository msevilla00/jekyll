---
layout: post
title:  "Mi primer post sobre Jekyll y Hugo"
date:   2020-12-27 02:00:00 +0200
categories: 
    - blog
    - pruebas
tags: 
    - jekyll
    - hugo
    - SSG
---

Este es es un post de prueba que estoy haciendo para incluir para mi yo del futuro lo que he ido aprendiendo de los llamados _SSGs_, _static site generators_, [Jekyll][Jekyll] y [Hugo][Hugo] y sobre los que he creado dos repositorios de pruebas en mi cuenta de [GitLab](https://gitlab.com/msevilla00):

- Repositorio de **Jekyll**: [msevilla00.gitlab.com/jekyll](https://msevilla00.gitlab.com/blog)
- Repositorio de **Hugo**: [msevilla00.gitlab.com/hugo](https://msevilla00.gitlab.com/hugo)

Extras por incluir

- Cada entrada tiene su campo de fecha (no tanto en nombre) que generará post solo si es en fecha pasada.
- El nombre del archivo puede que no coincida con la fecha de la cabecera pero ha de seguir el formato `AAAA-MM-DD-titulo-del-post.md`

[Jekyll]: https://jekyllrb.com/
[Hugo]: https://gohugo.io/

Los archivos extras, como imágenes que se pueden incluir dentro del directorio `assets` y referirse a ellos en los enlaces. Más info en [página de Jekyll](https://jekyllrb.com/docs/posts/#including-images-and-resources)

Los borradores se incluyen en la carpeta `_drafts` y pueden previsualizarse con: `jekyll server --watch --drafts`. Más info en [página de jekyll](https://jekyllrb.com/docs/posts/#drafts)

Otros enlaces de interés: 
- [Hugo or Jekyll: 6 Factors You Should Know - Forestry.io](https://forestry.io/blog/hugo-and-jekyll-compared/)
- [Running Jekyll in Docker – Davy's Tech Blog](https://ddewaele.github.io/running-jekyll-in-docker/)
- [Google Analytics setup for Jekyll by Michael Lee](https://michaelsoolee.com/google-analytics-jekyll/) & [Jekyll - How to NOT add Google Analytics when developing locally? - UHD Ed](https://uhded.com/jekyll-analytics-local-environment)
- [3 Simple steps to setup Jekyll Categories and Tags - Webjeda](https://blog.webjeda.com/jekyll-categories/)