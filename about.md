---
layout: page
title: About
permalink: /about/
---

Hola,

Soy Miguel Sevilla-Callejo, una persona entusiasta de las nuevas tecnologías[^1], las herramientas y los datos abiertos. Si quieres saber más de mi en el ámbito profesional y académico puedes echarle un ojo a mi currículum[^2].

Hace un tiempo que llevo pensando que debería actualizar y poner públicamente mis apuntes, materiales y demás pensamientos en una página web, blog o similar en internet, más allá de las notas personales que tengo en un repositorio privado en mi ordenador.

Esta página, blog o como quieras llamarla es un intento de lo anterior. Para empezar, me he lanzado a hacerlo en [Jekyll](https://jekyllrb.com/) pues me ha parecido lo más sencillo y directo. He usado la plantilla más básica, que no creo que dure mucho y no descarto pasar la página a [Hugo](https://gohugo.io/) pero por ahora creo que esto es más que suficiente.

En realidad esta página es más para mi yo del futuro y para tener un punto público para compartir algunas notas que un blog al uso pero cualquier persona es bienvenida a realizar comentarios o aportaciones (preferiblemente constructivas) del contenido que aquí tengo.

Todos la información aquí recogida, a no ser que se especifique lo contrario, está bajo licencia [Creative Commons — Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/), o lo que es lo mismo puedes usarla con toda libertad siempre que cites la fuente y si reutilizas esta información deberás realizar su publicación con la misma licencia.

_I'll try to translate part of this web/blog to English. Sometime ago I would do it but at this life time I do not have time to do it. So, my apologies to all English speakers who come here and find only this lines._

<br>

[^1]: En realidad muchas de estas tecnologías ya no son tan nuevas.
[^2]: Mi CV anda por ahí en varios lugares y lo enlazaré aquí en algún momento. 