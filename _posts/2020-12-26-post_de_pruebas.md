---
layout: post
title: "Post de pruebas"
date: 2020-12-26 10:00:00 +0200
categories: 
    - pruebas
    - blog
output: draft
---

## Renderizado de Markdown

# Título 1

El título 1 no lo renderiza bien (está en el título del post en cabecera)

## Título 2

### Título 3

#### Título 4

Texto de párrafo de prueba

<!-- esto es un comentario -->

En realidad el lenguaje usado para Jekyll es markdown pero reinterpretado por la biblioteca [kramdown](https://kramdown.gettalong.org/) escrito en Ruby.

Para más detalles de cómo escribir con markdown para el intérprete de ruby: [Quick Reference to kramdown](https://kramdown.gettalong.org/quickref.html)

kramdown
: biblioteca para interpretar y convertir lenguaje markdown

- Bloques

> A nice blockquote
{: #with-an-id}

- Notas al pie

This is a text with a footnote[^1].

[^1]: And here is the definition.


- Abreviaciones 
This is an HTML example.

*[HTML]: Hyper Text Markup Language

## Textos con códigos

`esto es texto de código`

- _Code snippets_

{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

- Markdown (triples acentos)

Lo mismo que lo anterior

```ruby
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
```

Ejemplo con BASH

```bash
# ir al directorio de datos
cd /home/user
rm -r /data02
# Otro comandos
rsync -av ./data01/ /media/user/hdext/bkps

```

## iframes

<iframe src="https://example.com" height="300" width="600" title="Example"></iframe> 

Código:
```html
 <iframe src="https://example.com" height="300" width="600" title="Example"></iframe> 
```
Más info sobre iframes: [HTML Iframes](https://www.w3schools.com/html/html_iframe.asp)